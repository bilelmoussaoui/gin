class ManifestNotFound(Exception):
    pass

class ParseError(Exception):
    pass
